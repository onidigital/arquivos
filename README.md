# Gerenciador de upload de arquivos + recorte em caso de imagens #


### Instalação ###

$ composer require onicmspack/arquivos
$ php artisan vendor:publish
$ php artisan migrate

- Criar o arquivo config/imagens.php caso não exista (já vai junto com o onicmsbase).

### Utilização ###

- Este pacote faz o upload das imagens na pasta storage, podendo ter subpastas para formatos diferentes.
- Para obter um arquivo basta dar {{ route('getfile', [id do arquivo, formato] ) }}
- Para obter as dimensões do arquivo/formato, é dessa forma:
- W: {{ config('imagens.nome_entidade.campo_imagem.formato.0') }}
- H: {{ config('imagens.nome_entidade.campo_imagem.formato.1') }}

Para galeria de fotos ainda não foi bolado nada.

- ver o pacote de slide como exemplo em caso de dúvidas

- [UPDATE] 30/06/2016
- Agora existe o método limpar_diretorio() que apaga arquivos(uploads efetuados pelo usuário, geralmente fotos) do servidor que não estão sendo utilizados, exemplo: um slide ão existe mais? apaga o arquivo referente a ele.
- Pode colocar via cron:

```
#!php
    # no app/Console/Kernel.php:
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $arq = new \Onicmspack\Arquivos\Models\Arquivo;
            $arq->limpar_diretorio();
        })->daily();
    }

```
- Não esquecer de inserir no servidor a tarefa:

```
#!php

* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```

- No mac dá pra simular:
- $ crontab -e, criar o arquivo com a nova linha e salvar