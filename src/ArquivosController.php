<?php 
namespace Onicmspack\Arquivos;

use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Illuminate\Routing\Controller as Controller;
use Storage;
use Illuminate\Http\Response;
 
class ArquivosController extends Controller
{

    // Retorna para a view um arquivo pelo seu id
    public function get($arquivo_id, $formato = '')
    {
    	if(!empty($formato))
    		$formato .= '/';

        $arquivo = Arquivo::where('id', '=', $arquivo_id)->first();
        $file = Storage::disk('local')->get($formato.$arquivo->nome);

        return (new Response($file, 200))
              ->header('Content-Type', $arquivo->mime_type);
    }
}



