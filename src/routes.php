<?php

Route::group(['middleware' => ['web'] ], function () {
	Route::get( '/arquivo/get/{arquivo_id}/{formato?}', [ 
		'as'   => 'getfile',
		'uses' => 'Onicmspack\Arquivos\ArquivosController@get'
		]
	);
});