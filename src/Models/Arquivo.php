<?php

namespace Onicmspack\Arquivos\Models;

use Storage, File, Image, DB;
use Intervention\Image\ImageManagerStatic as Intervention;
use Illuminate\Database\Eloquent\Model;

class Arquivo extends Model
{
    //https://www.codetutorial.io/laravel-5-file-upload-storage-download/
    //
    protected $fillable = ['nome',
    					   'legenda',
    					   'mime_type',
    					   'width',
    					   'height',
    					   'alt',
    					   'peso',
    					   ];

    // Configurações do recorte:
    public $formatos = [];
    public $chmod = 777;
    public $upload_dir; // onde os arquivos serão armazenados
    // arquivos para a exclusão automática ignorar:
    public $arquivos_excessao = ['.gitignore']; 

    public function __construct()
    {
        $this->upload_dir = storage_path('app');

        // colocar no cron na periodicidade desejada:
        //$this->limpar_diretorio();
    }

    /*
     *  Realiza o upload do(s) arquivo(s) e salva no BD
     */
    public function add($file)
    {
        $info = $file->getClientOriginalName();
        $filename  = pathinfo($info, PATHINFO_FILENAME);
        $extension = pathinfo($info, PATHINFO_EXTENSION);
        $filename  = str_slug($filename).'-'.rand(100,999);
        Storage::disk('local')->put($filename.'.'.$extension,  File::get($file));

        $arquivo = new Arquivo();
        $arquivo->mime_type = $file->getClientMimeType();
        $arquivo->nome      = $filename.'.'.$extension;
        $arquivo->alt       = $filename;

        // se for uma imagem, pega as informações disponíveis:
        if(substr($file->getMimeType(), 0, 5) == 'image') {
            list($width, $height) = getimagesize(storage_path('app/'.$arquivo->nome));
            $arquivo->width       = $width;
            $arquivo->height      = $height;
        }
 
        $arquivo->save();
        
        return $arquivo;
    }

    // Pega a imagem de $campo, $id e recorta
    public function recortar($entidade, $campo, $id)
    {
        $formato = 'imagens.'.$entidade.'.'.$campo;
        if( config($formato) === null ){
            die('Não foram encontradas as dimensões dessa entidade em config/imagens.php.');
        }

        // Obtem a imagem salva no bd:
        $file = Arquivo::find($id);
        if(!isset($file->id))
            return false;

        $filename = $file->nome;
        
        foreach(config($formato) as $pasta => $dimensoes){

            $pasta = $this->upload_dir.'/'.$pasta;

            // verifica se temos as dimensões:
            if(is_array($dimensoes)){

                // Verifica se existe a pasta, se não a cria:
                if(!file_exists($pasta)){
                    File::makeDirectory($pasta, $this->chmod, true);
                }

                if( ($dimensoes[0] > 0) && ($dimensoes[1] > 0)){
                    // Recorta nas dimenões exatas:
                    $manipulation = Intervention::make($this->upload_dir .'/'. $filename);
                    $manipulation->fit($dimensoes[0], $dimensoes[1]);
                }elseif( $dimensoes[0] > 0 ){
                    // Recorta levando em consideração apenas o width:
                    $manipulation = Intervention::make($this->upload_dir .'/'. $filename)->widen($dimensoes[0]);
                }elseif( $dimensoes[1] > 0 ){
                    // Recorta levando em consideração apenas o height:
                    $manipulation = Intervention::make($this->upload_dir .'/'. $filename)->heighten($dimensoes[1]);
                }
                $manipulation->save( $pasta. '/' . $filename);
            }else{
                die('As dimensões não são array. Pode ser [100, null] caso queira priorizar uma dimensão');
            }
        }
        // remove a imagem original:
        //@unlink($pasta.'/tmp/'.$filename);
        return TRUE;
    }

    /**
     * Método para remover arquivos obsoletos do projeto:
     * Ex: atualmente um slide é excluído mas o arquivo continua lá
     * O objetivo é fazer esta remoção automaticamente de todos os arquivos de entidades
     */
    public function limpar_diretorio()
    {
        // Atualiza a tabela de arquivos e retorna um array com os arquivos que estão em uso
        $arquivos = $this->atualiza_tabela_arquivos();

        if(! (count($arquivos) > 0) )
            return false;

        /**
         * Agora com a tabela arquivos atualizada, vamos aos diretórios e verificamos se
         * existem arquivos lá que não estão na tabela arquivos:
         */

        $files = File::allFiles($this->upload_dir);
        
        if(! (count($files) > 0) )
            return false;

        // Temos os arquivos, agora verificamos se eles estão no BD:
        foreach($files as $file){
            $arq = $file->getFilename();
            // se não está na lista de excessão e na lista do banco de dados, remove:
            if(!in_array($arq, $arquivos) && (!in_array($arq, $this->arquivos_excessao))){
                unlink((string)$file);
            }
        }
        // limpeza efetuada!
    }

    /**
     * Aqui remove registros de arquivos que não estão sendo usados em nenhuma tabela
     */
    public function atualiza_tabela_arquivos()
    {
        /**
         * Consultamos o arquivo de config.imagens que mapeia as entidades e seus arquivos
         * para remover do banco de arquivos os registros que não existem mais:

         | arquivos |                 |   slides   |
         ------------ <-------------> --------------
         |   id     |                 | arquivo_id |

         */

        // guardamos os arquivos para as verificações:
        $arquivos = DB::table('arquivos')->get();
        /** 
         * para cada registro em arquivos, verificamos se há correspondência dele em alguma entidade
         * se não houver, excluimos este registro
         */
        foreach($arquivos as $arquivo){
            // variavel que controla se está em uso o registro
            $esta_em_uso = false;
            foreach(config('imagens') as $entidade => $dados){

                // Busca nesta entidade a correspondência do arquivo atual:
                $query = DB::table($entidade)->limit(1);
                //
                $auxiliar = 0;
                // Depois de iniciada a consulta na entidade, verificamos cada campo que ela possui:
                foreach($dados as $campo => $valor){
                    if($auxiliar > 0)
                        $query->orWhere($campo, '=', $arquivo->id);
                    else
                        $query->Where($campo, '=', $arquivo->id);

                    $auxiliar = 1;
                }
                $resultado = $query->count();
                // Se encontrou, seta como true e passa pro próximo:
                if($resultado > 0){
                    $esta_em_uso = true;
                    continue;
                }
            }
            // se nenhuma entidade está utilizando este arquivo, deleta-o:
            if(!$esta_em_uso){
                DB::table('arquivos')->where('id', '=', $arquivo->id)->delete();
            }
        }

        // retorna os arquivos em uso:
        $arquivos = DB::table('arquivos')->lists('nome');
        return $arquivos;
    }

    function removeAcentosArq($str, $enc = "UTF-8"){

        $acentos = array(
        'A' => '/&Agrave;|&Aacute;|&Acirc;|&Atilde;|&Auml;|&Aring;/',
        'a' => '/&agrave;|&aacute;|&acirc;|&atilde;|&auml;|&aring;/',
        'C' => '/&Ccedil;/',
        'c' => '/&ccedil;/',
        'E' => '/&Egrave;|&Eacute;|&Ecirc;|&Euml;/',
        'e' => '/&egrave;|&eacute;|&ecirc;|&euml;/',
        'I' => '/&Igrave;|&Iacute;|&Icirc;|&Iuml;/',
        'i' => '/&igrave;|&iacute;|&icirc;|&iuml;/',
        'N' => '/&Ntilde;/',
        'n' => '/&ntilde;/',
        'O' => '/&Ograve;|&Oacute;|&Ocirc;|&Otilde;|&Ouml;/',
        'o' => '/&ograve;|&oacute;|&ocirc;|&otilde;|&ouml;/',
        'U' => '/&Ugrave;|&Uacute;|&Ucirc;|&Uuml;/',
        'u' => '/&ugrave;|&uacute;|&ucirc;|&uuml;/',
        'Y' => '/&Yacute;/',
        'y' => '/&yacute;|&yuml;/',
        'a.' => '/&ordf;/',
        'o.' => '/&ordm;/');

           return preg_replace($acentos,
                               array_keys($acentos),
                               htmlentities($str,ENT_NOQUOTES, $enc));
    }
}




















