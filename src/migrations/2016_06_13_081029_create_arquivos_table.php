<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArquivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arquivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('legenda')->nullable();
            $table->string('mime_type')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->string('alt')->nullable();
            $table->string('peso')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('arquivos');
    }
}
