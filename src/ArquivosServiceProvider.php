<?php

namespace Onicmspack\Arquivos;

use Illuminate\Support\ServiceProvider;

class ArquivosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Migrations
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/'),
        ]);

        // Configs
        /*
        $this->publishes([
            __DIR__.'/config' => config_path('arquivos.php'),
        ]);*/
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Onicmspack\Arquivos\ArquivosController');
    }
}
